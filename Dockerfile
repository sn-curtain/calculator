# Frontend build
FROM node:8-alpine as frontend

WORKDIR /home/app/sn-curtain.com-calculator
COPY . ./
WORKDIR /home/app/sn-curtain.com-calculator/frontend
RUN yarn install
RUN yarn build

# Runner build
FROM node:8-alpine

WORKDIR /home/app/sn-curtain.com-calculator

COPY . ./

RUN rm -rf frontend

RUN mkdir frontend

COPY --from=frontend /home/app/sn-curtain.com-calculator/frontend/ ./frontend

RUN yarn install

EXPOSE 5000

CMD [ "yarn", "start" ]
